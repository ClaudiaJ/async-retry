'use strict';

// Falsy value for tries causes loop infinitely
const defaults = function build({
    minTimeout = 1000,
    maxTimeout = 60 * 1000,
    factor = 2.0,
    tries = 6,
    strict = false
} = {}) {
    if (minTimeout > maxTimeout)
        throw new RangeError("minTimeout cannot exceed maxTimeout");
    if (!tries) console.warn("Retrying unto infinity");
    if (maxTimeout > 0x7fffffff) console.warn("Maximum timeout requested, clamping at ~24 days.");
    if (factor < 1) console.warn("Minimum factor is 1. Clamping at 1.")
    return {
        minTimeout,
        maxTimeout: Math.min(maxTimeout, 0x7fffffff),
        factor: Math.max(1, factor),
        tries,
        strict
    };
}

const randBetween = (a, b) => a + (b - a) * Math.random();

const strategies = {//{{{
    *linear(params) {
        const {minTimeout, maxTimeout, tries} = defaults(params);

        let [i, v] = [0, minTimeout];
        while (tries ? i < tries : !tries) {
            yield v = Math.floor(Math.min(maxTimeout, v + minTimeout));
            i += !!tries & i < Number.MAX_SAFE_INTEGER;
        }
        return;
    },
    *none(params) {
        const {minTimeout, tries} = defaults(params);

        let i = 0;
        while (tries ? i < tries : !tries) {
            yield minTimeout;
            i += !!tries & i < Number.MAX_SAFE_INTEGER;
        }
        return;
    },
    *exponential(params) {
        const {minTimeout, maxTimeout, factor, tries} = defaults(params);

        let i = 0;
        while (tries ? i < tries : !tries) {
            yield Math.floor(Math.min(maxTimeout, minTimeout * Math.pow(factor, i)));
            // Increment with clamp to max safe integer
            i += i < Number.MAX_SAFE_INTEGER;
        }
        return;
    },
    *fibonacci(params) {
        const {minTimeout, maxTimeout, tries} = defaults(params);

        var [a, b, i] = [minTimeout, minTimeout, 0];
        while (tries ? i < tries : !tries) {
            yield a;
            var [a, b] = [Math.min(b, maxTimeout), Math.min(a + b, maxTimeout)];
            i += !!tries & i < Number.MAX_SAFE_INTEGER;
        }
        return;
    },
    *jitterFull(params) {
        const {minTimeout, maxTimeout, factor, tries, strict} = defaults(params);

        let [v, exp] = [0, this.exponential({minTimeout, maxTimeout, factor, tries})];
        while (v = exp.next().value) {
            yield Math.floor(randBetween(strict ? 0 : minTimeout, v));
        }
        return;
    },
    *jitterEqual(params) {
        const {minTimeout, maxTimeout, factor, tries, strict} = defaults(params);

        let [v, exp] = [0, this.exponential({minTimeout, maxTimeout, factor, tries})];
        while (v = exp.next().value) {
            yield Math.floor(v / 2 + randBetween(strict ? 0 : minTimeout, v / 2));
        }
        return;
    },
    *jitterDecorrelated(params) {
        const {minTimeout, maxTimeout, tries} = defaults(params);

        let [i, sleep] = [0, minTimeout]
        while (tries ? i < tries : !tries) {
            yield sleep = Math.floor(Math.min(maxTimeout, randBetween(minTimeout, sleep * 3)));
            i += !!tries & i < Number.MAX_SAFE_INTEGER;
        }
        return;
    }
};//}}}

module.exports = {strategies, defaults};
